package com.pentagon.test;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button mBtnAvengers, mBtnRam, mBtnPost, mBtnLogin, mBtnTitanic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mBtnAvengers = findViewById(R.id.am_btn_avengers);
        mBtnRam = findViewById(R.id.am_btn_ram);
        mBtnPost = findViewById(R.id.am_btn_post);
        mBtnLogin = findViewById(R.id.am_btn_login);
        mBtnTitanic = findViewById(R.id.am_btn_titanic);

        mBtnAvengers.setOnClickListener(view -> startActivity(new Intent(MainActivity.this, AvengersActivity.class)));
        mBtnRam.setOnClickListener(view -> startActivity(new Intent(MainActivity.this, RamActivity.class)));
        mBtnPost.setOnClickListener(view -> startActivity(new Intent(MainActivity.this, ProfileActivity.class)));
        mBtnLogin.setOnClickListener(view -> startActivity(new Intent(MainActivity.this, LoginActivity.class)));
        mBtnTitanic.setOnClickListener(view -> startActivity(new Intent(MainActivity.this, TitanicActivity.class)));
    }
}












/*
Notes:
    List users: https://api.mocki.io/v1/b043df5a
    user: https://api.mocki.io/v1/ce5f60e2
    ram: https://rickandmortyapi.com/api/character/
    marvel: https://simplifiedcoding.net/demos/marvel/

    post: https://code.tutsplus.com/tutorials/sending-data-with-retrofit-2-http-client-for-android--cms-27845

*/