package com.pentagon.test;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.pentagon.test.Avengers.Avenger;
import com.pentagon.test.RickAndMorty.Ram;
import com.pentagon.test.RickAndMorty.RamApi;
import com.pentagon.test.RickAndMorty.Result;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RamActivity extends AppCompatActivity {

    private static final String TAG = "RamActivity";
    private TextView mText;
    private String result = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ram);
        mText = findViewById(R.id.ar_text);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RamApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RamApi ramApi = retrofit.create(RamApi.class);

        Call<Ram> call = ramApi.getCharacters();
        call.enqueue(new Callback<Ram>() {
            @Override
            public void onResponse(Call<Ram> call, Response<Ram> response) {
                Ram ram = response.body();
                List<Result> characters = ram.getResults();
                for (Result character : characters){
                    result+="\n\tName: " + character.getName();
                    result+="\n\tStatus: " + character.getStatus();
                    result+="\n\tSpecies: " + character.getSpecies();
                    result+="\n\tGender: " + character.getGender();
                    result+="\n\n";
                }
                runOnUiThread(() -> { mText.setText(result); });
            }

            @Override
            public void onFailure(Call<Ram> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());
                Toast.makeText(RamActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}