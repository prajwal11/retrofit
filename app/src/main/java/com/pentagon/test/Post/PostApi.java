package com.pentagon.test.Post;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface PostApi {
    String BASE_URL = "https://us-central1-neofin.cloudfunctions.net/neofin/";
    @POST("user/login/")
    @FormUrlEncoded
    Call<Result> loginUser(@Field("email") String email, @Field("password") String password);
}
