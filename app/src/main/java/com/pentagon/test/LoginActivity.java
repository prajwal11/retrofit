package com.pentagon.test;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;
import com.pentagon.test.Post.PostApi;
import com.pentagon.test.Post.Result;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivity";
    private TextInputLayout mEmailLayout, mPasswordLayout;
    private EditText mEmail, mPassword;
    private CardView mLogin;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mEmailLayout = findViewById(R.id.al_email_layout);
        mPasswordLayout = findViewById(R.id.al_password_layout);
        mEmail = findViewById(R.id.al_email);
        mPassword = findViewById(R.id.al_password);
        mLogin = findViewById(R.id.al_card_login);
        mProgressDialog = new ProgressDialog(LoginActivity.this);
        mEmail.addTextChangedListener(new MyTextWatcher(mEmail));
        mPassword.addTextChangedListener(new MyTextWatcher(mPassword));
        mLogin.setOnClickListener(view -> { submit(view); });
    }

    private void submit(View view){
        if (!validateEmail() || !isValidEmail())   return;
        if (!validatePassword())    return;
        hideKeyboard(view);
        login();
    }

    private void login(){
        String email = mEmail.getText().toString().trim();
        String password = mPassword.getText().toString().trim();
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.show();
        try {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(PostApi.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            PostApi postApi = retrofit.create(PostApi.class);
            Call<Result> call = postApi.loginUser(email, password);
            call.enqueue(new Callback<Result>() {
                @Override
                public void onResponse(Call<Result> call, Response<Result> response) {
                    try {
                        Result result = response.body();
                        if (result != null){
                            Toast.makeText(LoginActivity.this, result.getMessage(), Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(LoginActivity.this, ProfileActivity.class));
                        }else {
                            Toast.makeText(LoginActivity.this, "Null Response!", Toast.LENGTH_SHORT).show();
                        }
                    }catch (Exception e){
                        Log.d(TAG, "onResponse: Exception: " + e.getMessage());
                    }
                    mProgressDialog.dismiss();
                }

                @Override
                public void onFailure(Call<Result> call, Throwable t) {
                    Toast.makeText(LoginActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "onFailure: " + t.getMessage());
                    mProgressDialog.dismiss();
                }
            });
        }catch (Exception e){
            Log.d(TAG, "login: " + e.getMessage());
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            mProgressDialog.dismiss();
        }

    }

    private boolean validateEmail(){
        if (mEmail.getText().toString().trim().isEmpty() ){
            mEmailLayout.setError("Enter email");
            requestFocus(mEmailLayout);
            return false;
        }
        mEmailLayout.setErrorEnabled(false);
        return true;
    }

    private boolean validatePassword(){
        if (mPassword.getText().toString().trim().isEmpty()){
            mPasswordLayout.setError("Enter password");
            requestFocus(mPasswordLayout);
            return false;
        }
        mPasswordLayout.setErrorEnabled(false);
        return true;
    }

    private boolean isValidEmail(){
        String email = mEmail.getText().toString().trim();
        if (!TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            mEmailLayout.setErrorEnabled(false);
            return true;
        }
        mEmailLayout.setError("Incorrect format");
        requestFocus(mEmailLayout);
        return false;
    }

    private void requestFocus(View view){
        if (view.requestFocus()){
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void hideKeyboard(View v){
        InputMethodManager inputMethodManager = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(v.getApplicationWindowToken(),0);
    }

    public class MyTextWatcher implements TextWatcher{

        private View view;

        public MyTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            switch (view.getId()){
                case R.id.al_email:
                    validateEmail();
                    break;
                case R.id.al_password:
                    validatePassword();
                    break;
            }
        }
    }
}








