package com.pentagon.test;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.pentagon.test.Avengers.Avenger;
import com.pentagon.test.Avengers.AvengersApi;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AvengersActivity extends AppCompatActivity {

    private static final String TAG = "AvengersActivity";
    private TextView mText;
    private String result = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_avengers);
        mText = findViewById(R.id.aa_text);


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AvengersApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        AvengersApi avengersApi = retrofit.create(AvengersApi.class);
        Call<List<Avenger>> call = avengersApi.getAvengers();
        call.enqueue(new Callback<List<Avenger>>() {
            @Override
            public void onResponse(Call<List<Avenger>> call, Response<List<Avenger>> response) {
                List<Avenger> avengers = response.body();
                for (Avenger avenger : avengers){
                    result+="\n\tName: " + avenger.getName();
                    result+="\n\tReal Name: " + avenger.getRealName();
                    result+="\n\tBio: " + avenger.getBio();
                    result+="\n\n";
                }
                runOnUiThread(() -> { mText.setText(result); });
            }

            @Override
            public void onFailure(Call<List<Avenger>> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());
                Toast.makeText(AvengersActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}