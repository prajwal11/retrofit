package com.pentagon.test;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.pentagon.test.Titanic.Result;
import com.pentagon.test.Titanic.TitanicAPI;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class TitanicActivity extends AppCompatActivity {

    private static final String TAG = "TitanicActivity";
    private TextView mResult;
    private EditText mClass, mAge, mSex, mFare;
    private Button mSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_titanic);
        mResult = findViewById(R.id.at_result);
        mClass = findViewById(R.id.at_class);
        mAge = findViewById(R.id.at_age);
        mSex = findViewById(R.id.at_sex);
        mFare = findViewById(R.id.at_fare);
        mSubmit = findViewById(R.id.at_submit);
        mSubmit.setOnClickListener(view -> submit());
    }

    private void submit(){
        try {
            int pClass = Integer.parseInt(mClass.getText().toString().trim());
            int age = Integer.parseInt(mAge.getText().toString().trim());
            int sex = Integer.parseInt(mSex.getText().toString().trim());
            int fare = Integer.parseInt(mFare.getText().toString().trim());

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(TitanicAPI.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            TitanicAPI titanicAPI = retrofit.create(TitanicAPI.class);
            Call<Result> call  = titanicAPI.predict(pClass, sex, age, fare);
//            Call<Result> call  = titanicAPI.predict(1, 1, 20, 200);
            call.enqueue(new Callback<Result>() {
                @Override
                public void onResponse(Call<Result> call, Response<Result> response) {
                    try {
                        Result result = response.body();
                        String msg = result.getResult();
                        if (msg.equals("1"))    mResult.setText("Passenger will survive");
                        else if (msg.equals("0"))    mResult.setText("Passenger will not survive");
                        else    mResult.setText(msg);
                    }catch (Exception e){
                        Log.d(TAG, "onResponse: Exception: " + e.getMessage());
                        Toast.makeText(TitanicActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<Result> call, Throwable t) {
                    Log.d(TAG, "onFailure: error: " + t.getMessage());
                    Toast.makeText(TitanicActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }catch (Exception e){
            Log.d(TAG, "submit: error: " + e.getMessage());
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}
