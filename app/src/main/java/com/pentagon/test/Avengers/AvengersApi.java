package com.pentagon.test.Avengers;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface AvengersApi {
    String BASE_URL = "https://simplifiedcoding.net/demos/";

    @GET("marvel")
    Call<List<Avenger>> getAvengers();
}
