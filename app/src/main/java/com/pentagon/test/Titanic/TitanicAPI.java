package com.pentagon.test.Titanic;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface TitanicAPI {
    String BASE_URL = "https://prjvvl-api.herokuapp.com/";

    @POST("titanic/")
    @FormUrlEncoded
    Call<Result> predict(@Field("p_class") int pClass, @Field("sex") int sex, @Field("age") int age, @Field("fare") int fare);
}
