package com.pentagon.test.Titanic;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result {
    @SerializedName("result")
    @Expose
    private String result;

    public Result(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }
}
