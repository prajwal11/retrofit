package com.pentagon.test.RickAndMorty;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface RamApi {
    String BASE_URL = "https://rickandmortyapi.com/api/";

    @GET("character")
    Call<Ram> getCharacters();
}
