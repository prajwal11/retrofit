package com.pentagon.test;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.pentagon.test.Post.PostApi;
import com.pentagon.test.Post.Result;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PostActivity extends AppCompatActivity {

    private static final String TAG = "PostActivity";
    private TextView mResult;
    private EditText mEmail, mPassword;
    private Button mSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);
        mResult = findViewById(R.id.ap_text_result);
        mEmail = findViewById(R.id.ap_email);
        mPassword = findViewById(R.id.ap_password);
        mSubmit = findViewById(R.id.ap_button_submit);
        mSubmit.setOnClickListener(view -> { submit(); });
    }

    private void submit(){
        String email = mEmail.getText().toString().trim();
        String password = mPassword.getText().toString().trim();
        if (email.isEmpty() || password.isEmpty())   return;
        mResult.setText("Logging");
        try {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(PostApi.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            PostApi postApi = retrofit.create(PostApi.class);
            Call<Result> call =  postApi.loginUser(email, password);
            call.enqueue(new Callback<Result>() {
                @Override
                public void onResponse(Call<Result> call, Response<Result> response) {
                    try {
                        Result result = response.body();
                        if (result != null){
                            Log.d(TAG, "onResponse: " + result.getMessage());
                            mResult.setText(result.getMessage());
                        }else {
                            mResult.setText("Null response!");
                        }
                    }catch (Exception e){
                        Log.d(TAG, "onResponse: Exception: " + e.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<Result> call, Throwable t) {
                    Log.d(TAG, "onFailure: failed to load");
                }
            });
        }catch (Exception e){
            Log.d(TAG, "submit: Exception: " + e.getMessage());
        }

    }
}